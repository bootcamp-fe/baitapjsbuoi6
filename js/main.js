// Exercise 1

var ex1HanderlerBtn = document.getElementById("ex1-handler-btn");

function ex1Handler(){
    var n = 1;
    var sum = 0;

    while(sum<=10000){
        sum = sum + n;
        n++;
    }

    var resultEx1 = n - 1 ;


    document.querySelector('#ex1-result #ex-result-text').innerHTML = resultEx1;
}

// Exercise 2

function ex2Handler(){
var ex2Input1 = document.querySelector("#ex2-input #ex-input-1").value;
var ex2Input2 = document.querySelector("#ex2-input #ex-input-2").value;
var x = ex2Input1;
var n = ex2Input2;
    var sum = 0;
    for (var i = 1; i <= n ; i++){
        sum += Math.pow(x,i);
    }

    var resultEx2 = sum;
    document.querySelector('#ex2-result #ex-result-text').innerHTML = resultEx2;
}

// Exercise 3

function ex3Handler(){
    var ex3Input1 = document.querySelector("#ex3-input #ex-input-1").value;
    var n = ex3Input1;
    var result = 1;

    if(n > 0){
        for(var i = 1; i <= n ; i++){
            result *= i;
        }
    } else {
        result = "n không hợp lệ";
    }
    
    var resultEx3 = result;
    
    document.querySelector('#ex3-result #ex-result-text').innerHTML = resultEx3;
}

// Exercise 4

var ex4HanderlerBtn = document.getElementById("ex4-handler-btn");

function ex4Handler(){
    var ex4Input1 = document.querySelector("#ex4-input #ex-input-1").value;
    var addContentTo = document.getElementById("ex4-result");
    addContentTo.innerHTML = "";
    var n = ex4Input1;
    for ( var i = 1; i <= n ; i++ ){
        var  bgColor, innerText ;
        if(i%2==1){
            bgColor = "#3f84ed";
            innerText = "Div lẻ";
        } else {
            bgColor = "#fd3448";
            innerText = "Div chẵn";
        }
        addContentTo.innerHTML += '<div class="divle"style="width: 100%;background-color: '+ bgColor + ';color: white;padding: 8px 10px;">' + innerText + '</div>';
    }

    var resultEx4;
    document.querySelector('#ex4-result #ex-result-text').innerHTML = resultEx4;
}


// Exercise 5

function ex5Handler(){
    var ex5Input1 = document.querySelector("#ex5-input #ex-input-1").value;
    var n = ex5Input1;
    var resultString = ""  ;
    if(n > 1){
        for (var i = 2; i <= n ; i++){
            var iLaSoNguyenTo = true;
            for ( var j = 2; j < i; j++){
                if(i%j==0){
                    iLaSoNguyenTo = false;
                }
            }
            if(iLaSoNguyenTo){
                resultString = resultString + i + " ";
            }
        }
    } else {
        resultString = "Nhập vào n > 1"
    }
        var resultEx5 = resultString;
        document.querySelector('#ex5-result #ex-result-text').innerHTML = resultEx5;
    }